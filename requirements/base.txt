acct>=8.3.3,<9.0.0
dict-toolbox>=3.1.1
boto3>=1.25.0,<1.27.0
idem>=21.0.0,<25.0.0
pgpy>=0.6.0,<0.7.0
deepdiff>=6.3.0,<7.0.0
pop-serial>=1.1.0,<2.0.0
cryptography>=39.0.1,<42.0.0
