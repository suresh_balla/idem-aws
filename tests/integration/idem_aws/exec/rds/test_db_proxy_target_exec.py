"""Tests for validating Rds Db Proxy Targets."""
import uuid

import pytest


PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx, aws_rds_db_proxy, aws_rds_db_cluster):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    ret = await hub.exec.aws.rds.db_proxy_target.create(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        target_group_name="default",
        db_cluster_identifiers=[aws_rds_db_cluster.get("resource_id")],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_target.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert "default" in resource.get("resource_id")
    assert aws_rds_db_proxy.get("resource_id") in resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get", depends=["create"])
async def test_get(hub, ctx, aws_rds_db_proxy):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy_target.get(
        ctx, name=PARAMETER["name"], resource_id="invalid/resource/id/db-instance"
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.rds.db_proxy_target.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert "default" in resource.get("resource_id")
    assert aws_rds_db_proxy.get("resource_id") in resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="list", depends=["create"])
async def test_list(hub, ctx, aws_rds_db_proxy):
    r"""
    **Test function**
    """

    global PARAMETER
    ret = await hub.exec.aws.rds.db_proxy_target.list(
        ctx,
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        target_group_name="default",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource
        assert "default" in resource.get("resource_id")
        assert aws_rds_db_proxy.get("resource_id") in resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="delete", depends=["list"])
async def test_delete(hub, ctx, aws_rds_db_cluster):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy_target.delete(
        ctx,
        name="",
        resource_id=PARAMETER["resource_id"],
        db_cluster_identifiers=[aws_rds_db_cluster.get("resource_id")],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_target.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])
