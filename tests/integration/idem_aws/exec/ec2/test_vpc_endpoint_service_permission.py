"""Tests for validating EC2 VPC Endpoint Service Permissions."""
import uuid

import pytest


PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_create(hub, ctx, aws_vpc_endpoint_service, aws_iam_user):
    # Test - create new resource
    global PARAMETER

    assert (
        aws_vpc_endpoint_service
    ), "The vpc endpoint service might not have been created"
    assert aws_vpc_endpoint_service.get("resource_id")

    assert aws_iam_user, "The iam user might not have been created"
    assert aws_iam_user.get("arn")

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.create(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        add_allowed_principals=[aws_iam_user.get("arn")],
    )

    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["name"] == resource.get("name", None)

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        principal_arn=aws_iam_user.get("arn"),
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["name"] == resource.get("name", None)
    assert resource.get("principal_arn")
    assert aws_iam_user.get("arn") == resource.get("principal_arn")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get(hub, ctx, aws_vpc_endpoint_service, aws_iam_user):
    # Test - Invalid/Not-Found resource lookup
    global PARAMETER

    if not hub.tool.utils.is_running_localstack(ctx):
        service_id = aws_vpc_endpoint_service.get("resource_id")
    else:
        # localstack does not support filter on principal_arn for operation DescribeVpcEndpointServicePermissions
        # So, it will use invalid_service_id to replicate NotFound
        service_id = "invalid_service_id"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
        ctx,
        name=PARAMETER["name"],
        service_id=service_id,
        principal_arn="invalid_user_arn",
    )

    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        principal_arn=aws_iam_user.get("arn"),
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["name"] == resource.get("name", None)
    assert resource.get("principal_arn")
    assert aws_iam_user.get("arn") == resource.get("principal_arn")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_list(hub, ctx, aws_vpc_endpoint_service):
    global PARAMETER
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.list(
        ctx,
        service_id=aws_vpc_endpoint_service.get("resource_id"),
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource
        assert resource.get("principal_arn")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update(hub, ctx, aws_vpc_endpoint_service, aws_iam_user, aws_iam_user_2):
    # Test - Update existing resource
    global PARAMETER

    # Add new user as allowed principal
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.update(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        add_allowed_principals=[aws_iam_user_2.get("arn")],
    )

    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Remove existing user from allowed principal
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.update(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        remove_allowed_principals=[aws_iam_user.get("arn")],
    )

    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # localstack does not support filter on invalid principal_arn
    if not hub.tool.utils.is_running_localstack(ctx):
        # Now get the resource for removed principal
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
            ctx,
            name=PARAMETER["name"],
            service_id=aws_vpc_endpoint_service.get("resource_id"),
            principal_arn=aws_iam_user.get("arn"),
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert "result is empty" in str(ret["comment"])

    # Now get the resource for added principal
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        principal_arn=aws_iam_user_2.get("arn"),
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["name"] == resource.get("name", None)
    assert resource.get("principal_arn")
    assert aws_iam_user_2.get("arn") == resource.get("principal_arn")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx, aws_vpc_endpoint_service, aws_iam_user_2):
    # Test - Delete existing principal
    global PARAMETER

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.delete(
        ctx,
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        remove_allowed_principals=[aws_iam_user_2.get("arn")],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        principal_arn=aws_iam_user_2.get("arn"),
    )

    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])
