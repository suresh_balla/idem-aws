import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-scaling-policy-" + str(int(time.time())),
    "policy_name": "idem-test-policy-" + str(int(time.time())),
    "service_namespace": "rds",
    "scalable_dimension": "rds:cluster:ReadReplicaCount",
    "policy_type": "StepScaling",
    "step_scaling_policy_configuration": {
        "AdjustmentType": "PercentChangeInCapacity",
        "StepAdjustments": [
            {
                "MetricIntervalLowerBound": 0,
                "MetricIntervalUpperBound": 15,
                "ScalingAdjustment": 1,
            },
            {
                "MetricIntervalLowerBound": 15,
                "MetricIntervalUpperBound": 25,
                "ScalingAdjustment": 2,
            },
            {"MetricIntervalLowerBound": 25, "ScalingAdjustment": 3},
        ],
        "MinAdjustmentMagnitude": 1,
        "Cooldown": 20,
        "MetricAggregationType": "Average",
    },
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling policy creation is not supported in localstack.",
)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_rds_db_cluster, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER[
        "scaling_resource_id"
    ] = f"cluster:{aws_rds_db_cluster.get('resource_id')}"
    # we have to register the scaling target before scaling policy can be applied.
    register_scaling_policy = (
        await hub.states.aws.application_autoscaling.scalable_target.present(
            ctx,
            name="idem-test-scaling-target-" + str(int(time.time())),
            service_namespace=PARAMETER["service_namespace"],
            scaling_resource_id=PARAMETER["scaling_resource_id"],
            scalable_dimension=PARAMETER["scalable_dimension"],
            min_capacity=1,
            max_capacity=3,
        )
    )
    ret = await hub.states.aws.application_autoscaling.scaling_policy.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.application_autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.application_autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_scaling_policy(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling policy creation is not supported in localstack.",
)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.application_autoscaling.scaling_policy.describe(
        ctx
    )
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert (
        "aws.application_autoscaling.scaling_policy.present"
        in describe_ret[resource_id]
    )
    described_resource = describe_ret[resource_id].get(
        "aws.application_autoscaling.scaling_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_scaling_policy(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling policy creation is not supported in localstack.",
)
@pytest.mark.dependency(name="modify_step_scaling_configuration", depends=["describe"])
async def test_modify_step_scaling_configuration(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["step_scaling_policy_configuration"] = {
        "AdjustmentType": "PercentChangeInCapacity",
        "StepAdjustments": [
            {
                "MetricIntervalLowerBound": 0,
                "MetricIntervalUpperBound": 20,
                "ScalingAdjustment": 1,
            },
            {
                "MetricIntervalLowerBound": 20,
                "MetricIntervalUpperBound": 30,
                "ScalingAdjustment": 2,
            },
            {"MetricIntervalLowerBound": 30, "ScalingAdjustment": 3},
        ],
        "MinAdjustmentMagnitude": 1,
        "Cooldown": 60,
        "MetricAggregationType": "Maximum",
    }
    ret = await hub.states.aws.application_autoscaling.scaling_policy.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.application_autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.application_autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_scaling_policy(ret["old_state"], PARAMETER)
    assert_scaling_policy(ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling policy creation is not supported in localstack.",
)
@pytest.mark.dependency(name="absent", depends=["modify_step_scaling_configuration"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.application_autoscaling.scaling_policy.absent(
        ctx,
        name=PARAMETER["name"],
        policy_name=PARAMETER["policy_name"],
        scaling_resource_id=PARAMETER["scaling_resource_id"],
        service_namespace=PARAMETER["service_namespace"],
        scalable_dimension=PARAMETER["scalable_dimension"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_scaling_policy(ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.application_autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.application_autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling policy creation is not supported in localstack.",
)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.application_autoscaling.scaling_policy.absent(
        ctx,
        name=PARAMETER["name"],
        policy_name=PARAMETER["policy_name"],
        scaling_resource_id=PARAMETER["scaling_resource_id"],
        service_namespace=PARAMETER["service_namespace"],
        scalable_dimension=PARAMETER["scalable_dimension"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.application_autoscaling.scaling_policy",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
async def test_scaling_policy_absent_with_none_resource_id(hub, ctx):
    scaling_policy_temp_name = "idem-test-scaling_policy-" + str(int(time.time()))
    # Delete scaling_policy with resource_id as None. Result in no-op.
    ret = await hub.states.aws.application_autoscaling.scaling_policy.absent(
        ctx,
        name=scaling_policy_temp_name,
        policy_name=PARAMETER["policy_name"],
        scaling_resource_id=PARAMETER["scaling_resource_id"],
        service_namespace=PARAMETER["service_namespace"],
        scalable_dimension=PARAMETER["scalable_dimension"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.application_autoscaling.scaling_policy",
            name=scaling_policy_temp_name,
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.application_autoscaling.scaling_policy.absent(
            ctx,
            name=PARAMETER["name"],
            policy_name=PARAMETER["policy_name"],
            scaling_resource_id=PARAMETER["scaling_resource_id"],
            service_namespace=PARAMETER["service_namespace"],
            scalable_dimension=PARAMETER["scalable_dimension"],
            resource_id=None,
        )
        assert ret["result"], ret["comment"]


def assert_scaling_policy(resource, parameters):
    assert parameters.get("service_namespace") == resource.get("service_namespace")
    assert parameters.get("scaling_resource_id") == resource.get("scaling_resource_id")
    assert parameters.get("scalable_dimension") == resource.get("scalable_dimension")
    assert parameters.get("policy_name") == resource.get("policy_name")
    assert parameters.get("policy_type") == resource.get("policy_type")
    if parameters.get("policy_type") == "StepScaling":
        assert parameters.get("step_scaling_policy_configuration") == resource.get(
            "step_scaling_policy_configuration"
        )
    else:
        assert parameters.get(
            "target_tracking_scaling_policy_configuration"
        ) == resource.get("target_tracking_scaling_policy_configuration")
