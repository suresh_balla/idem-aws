import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-resource-" + str(int(time.time())),
    "http_method": "GET",
    "status_code": "400",
    "content_handling": "CONVERT_TO_TEXT",
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_apigateway_integration, cleanup):
    global PARAMETER
    rest_api_id = aws_apigateway_integration.get("rest_api_id")
    parent_resource_id = aws_apigateway_integration.get("parent_resource_id")

    http_method = PARAMETER["http_method"]
    status_code = PARAMETER["status_code"]
    PARAMETER["parent_resource_id"] = parent_resource_id
    PARAMETER["rest_api_id"] = rest_api_id
    resource_id = f"{rest_api_id}-{parent_resource_id}-{http_method}-{status_code}"

    ctx["test"] = __test
    present_ret = await hub.states.aws.apigateway.integration_response.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.apigateway.integration_response",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource_id
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.apigateway.integration_response",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["status_code"] == resource.get("status_code")
    assert PARAMETER["content_handling"] == resource.get("content_handling")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.integration_response.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.integration_response.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.apigateway.integration_response.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["status_code"] == described_resource_map.get("status_code")
    assert PARAMETER["http_method"] == described_resource_map.get("http_method")
    assert PARAMETER["parent_resource_id"] == described_resource_map.get(
        "parent_resource_id"
    )
    assert PARAMETER["rest_api_id"] == described_resource_map.get("rest_api_id")
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigateway.integration_response.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["status_code"] == resource.get("status_code")
    assert PARAMETER["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-invalid-resource-id", depends=["present"])
async def test_get_invalid_resource_id(hub, ctx):
    resource_id = "fake-id"
    ret = await hub.exec.aws.apigateway.integration_response.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"] is False, ret["comment"]
    assert ret["ret"] is None
    assert f"Invalid Resource ID '{resource_id}'." in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-not-found", depends=["present"])
async def test_get_resource_id_does_not_exist(hub, ctx):
    resource_id = "restApiId-resourceId-httpMethod-400"
    ret = await hub.exec.aws.apigateway.integration_response.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get '{resource_id}' result is empty" and f"NotFoundException" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["content_handling"] = "CONVERT_TO_BINARY"
    ret = await hub.states.aws.apigateway.integration_response.present(
        ctx, **new_parameter
    )

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.integration_response",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert new_parameter["name"] == resource["name"]
        assert new_parameter["resource_id"] == resource["resource_id"]
        assert new_parameter["rest_api_id"] == resource["rest_api_id"]
        assert new_parameter["parent_resource_id"] == resource["parent_resource_id"]
        assert new_parameter["http_method"] == resource["http_method"]
        assert new_parameter["status_code"] == resource["status_code"]
        assert new_parameter["content_handling"] == resource["content_handling"]
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.apigateway.integration_response",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["present"])
async def test_absent_integration_response(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.integration_response.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["status_code"] == resource.get("status_code")
    assert PARAMETER["name"] == resource.get("name")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.integration_response",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.integration_response",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.integration_response.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"]
    assert ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.integration_response",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_integration_response_absent_with_none_resource_id(hub, ctx):
    "idem-integration-response" + str(int(time.time()))
    # Delete integration response with resource_id as None. Result in no-op.
    ret = await hub.states.aws.apigateway.integration_response.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.integration_response",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigateway.integration_response.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
