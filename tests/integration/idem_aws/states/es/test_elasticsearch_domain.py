import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
NAME = "test-domain-" + str(int(time.time()))
PARAMETER = {
    "name": NAME,
    "domain_name": NAME,
    "elastic_search_version": "6.8",
    "ebs_options": {
        "EBSEnabled": True,
        "Iops": 0,
        "VolumeSize": 10,
        "VolumeType": "gp2",
    },
    "advanced_security_options": {
        "AnonymousAuthEnabled": False,
        "Enabled": False,
        "InternalUserDatabaseEnabled": False,
    },
    "tags": {"test-tag": "idem-create", "Name": NAME},
}
comment_utils_kwargs = {
    "resource_type": "aws.es.elasticsearch_domain",
    "name": NAME,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False, "localstack/Pro does not support elastic search domain ")
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test

    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]

    ret = await hub.states.aws.es.elasticsearch_domain.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")

    if __test:
        assert would_create_message in ret["comment"]
    else:
        PARAMETER["resource_id"] = NAME
        assert created_message in ret["comment"]

    assert not ret.get("old_state") and ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["elastic_search_version"] == resource.get("elastic_search_version")
    assert PARAMETER["ebs_options"] == resource.get("ebs_options")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.es.elasticsearch_domain.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    resource = dict(
        ChainMap(*describe_ret[resource_id].get("aws.es.elasticsearch_domain.present"))
    )

    assert PARAMETER["domain_name"] in resource.get("domain_name")
    assert PARAMETER["elastic_search_version"] == resource.get("elastic_search_version")
    assert PARAMETER["ebs_options"] == resource.get("ebs_options")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.es.elasticsearch_domain.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["domain_name"] in resource.get("domain_name")
    assert PARAMETER["elastic_search_version"] == resource.get("elastic_search_version")
    assert PARAMETER["ebs_options"] == resource.get("ebs_options")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-tags", depends=["present"])
async def test_update_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    # update tag 'test-tag', add tag 'new-tag', remove tag 'Name'
    new_parameter["tags"] = {"test-tag": "idem-update", "new-tag": "new-value"}
    ret = await hub.states.aws.es.elasticsearch_domain.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert old_resource.get("tags")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["elastic_search_version"] == old_resource.get(
        "elastic_search_version"
    )

    removed, added = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags=PARAMETER["tags"], new_tags=new_parameter["tags"]
    )
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_tags_comment(removed, added)[0]
            in ret["comment"]
        )

    resource = ret.get("new_state")
    assert new_parameter["name"] == resource.get("name")
    assert resource.get("tags")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["elastic_search_version"] == resource.get(
        "elastic_search_version"
    )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["update-tags"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["ebs_options"] = {
        "EBSEnabled": True,
        "Iops": 0,
        "VolumeSize": 10,
        "VolumeType": "standard",
    }
    would_update_message = hub.tool.aws.comment_utils.would_update_comment(
        **comment_utils_kwargs
    )[0]
    update_message = hub.tool.aws.comment_utils.update_comment(**comment_utils_kwargs)[
        0
    ]

    ret = await hub.states.aws.es.elasticsearch_domain.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    if __test:
        assert would_update_message in ret["comment"]
    else:
        assert update_message in ret["comment"]

    resource = ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["elastic_search_version"] == resource.get("elastic_search_version")
    assert PARAMETER["ebs_options"] == resource.get("ebs_options")
    assert PARAMETER["advanced_security_options"] == resource.get(
        "advanced_security_options"
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]

    ret = await hub.states.aws.es.elasticsearch_domain.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    if __test:
        assert would_delete_message in ret["comment"]
    else:
        assert deleted_message in ret["comment"]

    assert ret.get("old_state") and not ret.get("new_state")
    old_resource = ret.get("old_state")
    resource = dict(ChainMap(old_resource))
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["domain_name"] == resource.get("domain_name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["elastic_search_version"] == resource.get("elastic_search_version")
    assert PARAMETER["ebs_options"] == resource.get("ebs_options")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]

    ret = await hub.states.aws.es.elasticsearch_domain.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert already_absent_message in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_absent_with_none_resource_id(hub, ctx):
    temp_name = "idem-test-elasticsearch-domain-" + str(int(time.time()))
    # Delete with resource_id as None. Should result in no-op.
    ret = await hub.states.aws.es.elasticsearch_domain.absent(
        ctx,
        name=temp_name,
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        resource_type="aws.es.elasticsearch_domain",
        name=temp_name,
    )[0]
    assert already_absent_message in ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if not hub.tool.utils.is_running_localstack(ctx):
        if "resource_id" in PARAMETER:
            ret = await hub.states.aws.es.elasticsearch_domain.absent(
                ctx,
                name=PARAMETER["name"],
                resource_id=PARAMETER["resource_id"],
            )
            assert ret["result"], ret["comment"]
