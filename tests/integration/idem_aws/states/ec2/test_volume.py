import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


@pytest.fixture(scope="module")
def shared_data():
    PARAMETER = {
        "name": "idem-test-volume-" + str(int(time.time())),
        "encrypted": True,
        "volume_type": "standard",
        "size": 1,
    }
    return PARAMETER


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup, aws_availability_zone, shared_data):
    shared_data["availability_zone"] = aws_availability_zone
    ctx["test"] = __test
    shared_data["tags"] = {"Name": shared_data["name"]}

    ret = await hub.states.aws.ec2.volume.present(
        ctx,
        **shared_data,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert f"Would create aws.ec2.volume '{shared_data['name']}'" in ret["comment"]
    else:
        shared_data["resource_id"] = resource["resource_id"]
        assert f"Created aws.ec2.volume '{shared_data['name']}'" in ret["comment"]
    assert not ret["old_state"], ret["comment"]
    assert ret["new_state"], ret["comment"]
    assert shared_data["encrypted"] == resource.get("encrypted")
    assert shared_data["tags"] == resource.get("tags")
    assert shared_data["name"] == resource.get("name")
    assert shared_data["size"] == resource.get("size")
    assert shared_data["volume_type"] == resource.get("volume_type")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx, shared_data):
    describe_ret = await hub.states.aws.ec2.volume.describe(ctx)
    resource_id = shared_data["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.volume.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.ec2.volume.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert shared_data["tags"] == described_resource_map.get("tags")
    assert shared_data["size"] == described_resource_map.get("size")
    assert shared_data["encrypted"] == described_resource_map.get("encrypted")
    assert shared_data["volume_type"] == described_resource_map.get("volume_type")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test, shared_data):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=shared_data["name"], resource_id=shared_data["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["comment"]
    assert not ret["new_state"], ret["comment"]
    old_resource = ret["old_state"]
    assert shared_data["tags"] == old_resource.get("tags")
    assert shared_data["volume_type"] == old_resource.get("volume_type")
    assert shared_data["size"] == old_resource.get("size")
    assert shared_data["encrypted"] == old_resource.get("encrypted")
    if __test:
        assert f"Would delete aws.ec2.volume '{shared_data['name']}'" in ret["comment"]
    else:
        assert f"Deleted aws.ec2.volume '{shared_data['name']}'" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test, shared_data):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=shared_data["name"], resource_id=shared_data["resource_id"]
    )
    assert ret["result"], ret["comment"]
    # assert not ret["old_state"], ret["comment"]
    assert not ret["new_state"], ret["comment"]
    if not __test:
        assert (
            f"aws.ec2.volume '{shared_data['name']}' already absent" in ret["comment"]
        )
        shared_data.pop("resource_id")


@pytest.mark.asyncio
async def test_absent_with_none_resource_id(hub, ctx):
    volume_temp_name = "idem-test-volume-" + str(int(time.time()))
    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=volume_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"], ret["comment"]
    assert not ret["new_state"], ret["comment"]
    assert f"aws.ec2.volume '{volume_temp_name}' already absent" in ret["comment"]


@pytest.fixture(scope="module")
async def cleanup(hub, ctx, shared_data):
    yield None
    if "resource_id" in shared_data:
        ret = await hub.states.aws.ec2.volume.absent(
            ctx, name=shared_data["name"], resource_id=shared_data["resource_id"]
        )
        assert ret["result"], ret["comment"]
